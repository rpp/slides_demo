---
layout: slide
title: Título da Apresentação
description: Breve descrição vem aqui
theme: beige
transition: slide
permalink: index.html
---

<section data-markdown>
# Título da Apresentação

## Subtítulo

### Autor

</section>



<section data-markdown>
## Para começar

Modifique esta apresentação

```
$ git clone https://rpp@gitlab.com/rpp/slides_demo.git
$ cd slides_demo
$ bundle exec jekyll serve
```
</section>


<section data-markdown>
## Adicione figuras!

![Figura   1](http://cta.if.ufrgs.br/attachments/download/2148)
</section>



<section data-markdown>
## Mais opções

* Temas
* Listas
* Tabelas
* MathJax
* Transições diversas
* ...

</section>


<section data-markdown>
## Baseado em Reveal.js

Desenvolvido por Hakim El Hattab

Mais informações em http://lab.hakim.se/reveal-js/

</section>
